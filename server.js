// server.js
const next = require('next');
const routes = require('./assets/helpers/routes');
const app = next({dev: process.env.NODE_ENV !== 'production'});
const cors = require('cors');
const handler = routes.getRequestHandler(app);
// With express
const express = require('express');

app.prepare().then(() => {
    const port = 3000;
    express().use(cors);
    express().use(handler).listen(port, err => {
        if (err) throw err;
        console.log('> Ready on http://localhost:' + port);
    });
});