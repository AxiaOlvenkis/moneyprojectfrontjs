import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "../user/forms.scss";
import AuthToken from "../../assets/helpers/auth_token";
import axios from 'axios';
import {Link, Router} from "../../assets/helpers/routes";
import Societe from "../../components/Fiche/Societe";
import Filiales from "../../components/Fiche/Filiales";
import Filiale from "../../components/Fiche/Filiale";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            societe: null,
            kFiliale: null,
            filiale: null,
            home: true,
            error: "",
            success: ""
        };
    }

    componentDidMount(){
        const auth = new AuthToken();
        if(!auth.isAuth()){
            Router.pushRoute('login')
        }else if(!auth.isComplete()){
            Router.pushRoute('complete_account');
        }

        this._updateData();
    }

    _updateData = () => {
        const { filiale, kFiliale } = this.state;
        const auth = new AuthToken();

        // on recupere la societe
        // on set les infos
        const id = auth.getSociety();
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
            }
        };

        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/societes/${id}`, config).then((response) => {
            const societe = response.data;
            let fil = null;

            if(filiale){
                fil = societe.societeFiliales[kFiliale]
            }

            this.setState({
                'societe': societe,
                'filiale': fil
            });
        }).catch((error) => {
            return null;
        });
    }

    lineClick = (key) => {
        const fil = this.state.societe.societeFiliales[key];
        this.setState({
            kFiliale: key,
            filiale: fil
        });
    }

    backBtnClick = () => {
        this.setState({
            kFiliale: null,
            filiale: null
        });
    }

    render(){
        const { societe, filiale } = this.state;

        return (
            <React.Fragment>
                <Head>
                    <title>Tableau de Bord - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Tableau de Bord</h1>

                    {!societe
                        ? ""
                        : <Societe societe={societe} />
                    }

                    <hr />

                    <div className="row">
                        <div className="col-12">
                            {!societe
                                ? ""
                                : <Link route={ 'create-filiale' }>Créer une filiale</Link>
                            }
                        </div>
                        <div className="col-12">
                            {
                                !filiale
                                    ? <DashboardHome societe={ societe } lineClick={ this.lineClick } />
                                    : <Filiale filiale={ filiale } societe={ societe } backClick={ this.backBtnClick } onUpdate={ this._updateData } />
                            }
                        </div>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}

class DashboardHome extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        const { societe } = this.props;

        return (
            <React.Fragment>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Budget</th>
                        <th>Ville</th>
                        <th>Type</th>
                        <th>Niveau</th>
                        <th>Production</th>
                    </tr>
                    </thead>
                    <tbody>
                    {!societe || !societe.societeFiliales.length
                        ? ""
                        : societe.societeFiliales.map((filiale, index) => (
                            <Filiales key={index} {...filiale} lineClick={() => this.props.lineClick(index)} />
                        ))}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default Dashboard;