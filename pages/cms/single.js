import React from 'react'
import Layout from '../../components/Layout';
import { withRouter } from 'next/router';
import Head from "next/head";
import axios from 'axios';

class Page extends React.Component {

	static getInitialProps = async ({query}) => {
		const res = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/noauth/pages/slug/${query.slug}`);
		const data = await res.data;

		return { details: data };
	};

  	render = () => {
		const { title, contenu } = this.props.details;

	    return (
			<React.Fragment>
				<Head>
					<title>{ title } - Money Project</title>
				</Head>

				<Layout>
					<h1>{ title }</h1>
					<div className="container" dangerouslySetInnerHTML={{ __html: contenu }} />
				</Layout>
			</React.Fragment>
	    )
	}
}
export default withRouter(Page)