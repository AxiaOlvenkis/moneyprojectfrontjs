import Layout from '../../components/Layout';
import React from "react";
import {withRouter} from "next/router";
import Head from "next/head";
import {Link} from "../../assets/helpers/routes";
import axios from 'axios';
import "./liste.scss";


class Changelogs extends React.Component {

    static getInitialProps = async () => {
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };

        const res = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/noauth/changelogs?order[published_at]=desc`, headers);
        const data = await res.data;

        return { liste: data };
    };

    render = () => {
        const { liste } = this.props;

        return (
            <React.Fragment>
                <Head>
                    <title>Changelogs - Money Project</title>
                </Head>
                <Layout>
                    <h1>Changelogs</h1>

                    <div className="liste container">
                        {liste.map(change => {
                            const date = new Date(change.published_at);
                            return (
                                <React.Fragment>
                                    <div className="single">
                                        <h4>
                                            { change.intitule },
                                            <span className="date">publié le
                                                <span>{new Intl.DateTimeFormat("fr-FR", {
                                                    year: "numeric",
                                                    month: "long",
                                                    day: "2-digit"
                                                }).format(date)}</span></span>
                                        </h4>
                                        <div className="items">
                                            {
                                                change.changelogBlocks.map(bloc => {
                                                    return(
                                                        <React.Fragment>
                                                            <div className="bloc">
                                                                <span className="bloc-title">{ bloc.titre }</span>
                                                                <div className="bloc-content" dangerouslySetInnerHTML={{ __html: bloc.texte }} />
                                                            </div>
                                                        </React.Fragment>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                </React.Fragment>
                            )
                        })}
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}
export default withRouter(Changelogs)