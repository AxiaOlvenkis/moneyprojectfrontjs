import Layout from '../../components/Layout';
import React from "react";
import {withRouter} from "next/router";
import Head from "next/head";
import {Link} from "../../assets/helpers/routes";
import axios from 'axios';
import "./liste.scss";


class Todo extends React.Component {

    static getInitialProps = async () => {
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };

        const res = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/noauth/todo?order[priority]=asc`, headers);
        const data = await res.data;

        return { liste: data };
    };

    render = () => {
        const { liste } = this.props;
        console.log(liste);

        return (
            <React.Fragment>
                <Head>
                    <title>Todo Liste - Money Project</title>
                </Head>
                <Layout>
                    <h1>Todo Liste</h1>

                    <div className="todoliste">
                        {
                            liste.map((todo, key) => {
                                return(
                                    <React.Fragment>
                                        <div className="item" key={ key }>
                                            <span className="title h4">{ todo.libelle }</span>
                                            <div className="description" dangerouslySetInnerHTML={{ __html: todo.description }} />
                                            <div className="tags">
                                                {
                                                    todo.tags.map((tag, key) => {
                                                        return(
                                                            <span key={ key } className="badge" style={{ backgroundColor: tag.couleurs }}>{ tag.libelle }</span>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )
                            })
                        }
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}
export default withRouter(Todo)