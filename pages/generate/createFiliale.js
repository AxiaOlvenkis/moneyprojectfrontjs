import React from "react";
import Head from "next/head";
import Layout from "../../components/Layout";
import './Create.scss';
import {IoIosArrowRoundBack} from "react-icons/io";
import { Router } from "../../assets/helpers/routes";
import AddFilialeForm from "../../components/Forms/AddFilialeForm";
import AuthToken from "../../assets/helpers/auth_token";
import axios from "axios";

class CreateFiliale extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,
            societe: null
        };
    }

    componentDidMount() {
        const auth = new AuthToken();
        this.setState({
            societe: auth.getSociety()
        })
    }

    _handleSubmit = async (event,dataForm) => {
        event.preventDefault();
        this.setState({
            alert: null
        });
        const auth = new AuthToken();

        const data = new FormData();
        data.set('societe', auth.getSociety());
        data.set('filiale', dataForm['sFiliale']);
        data.set('user', auth.getUserID());
        data.set('ville', dataForm['ville']);
        data.set('type', dataForm['type']);
        data.set('ressource', dataForm['ressource']);
        console.log(data);

        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Content-Type': 'application/merge-patch+json'
            }
        };
        const res = await axios.post(`${ process.env.NEXT_PUBLIC_API_URL }/filiale`, data, config).then((response) => {
            return response;
        }).catch((error) => {
            return null;
        });

        if(res === null){
            this.setState({ 'error' : "Une erreur est survenue lors de l'envoi au serveur." });
        }else{
            this.setState({ 'success' : 'La filiale a bien été crée.' });
            Router.pushRoute('dashboard');
        }
    }

    render = () => {
        const { alert, societe } = this.state;

        return (
            <React.Fragment>
                <Head>
                    <title>Créer une nouvelle filiale - Money Project</title>
                </Head>
                <Layout>
                    <div className="create-filiale-header">
                        <IoIosArrowRoundBack onClick={ Router.back } className="back-btn" />
                        <h1 className="text-center">Créer une nouvelle filiale</h1>
                    </div>

                    <div className="create-filiale-content">
                        <AddFilialeForm alert={ alert } handleSubmit={ this._handleSubmit } societe={ societe }/>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}
export default CreateFiliale