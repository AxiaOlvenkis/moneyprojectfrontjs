import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "./forms.scss";
import AuthToken from "../../assets/helpers/auth_token";
import AddFilialeForm from "../../components/Forms/AddFilialeForm";
import axios from "axios";
import { Router } from '../../assets/helpers/routes';

class Compte extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            types: [],
            villes: [],
            ressources: [],
            sSociete: "",
            sFiliale: "",
            sVille: "",
            sType: "",
            sRessource: 0,
            ids: [],
            alert: null
        };
    }

    _handleSubmit = async (event,dataForm) => {
        event.preventDefault();
        this.setState({
            alert: null
        });
        const auth = new AuthToken();

        const data = new FormData();
        data.set('societe', dataForm['sSociete']);
        data.set('filiale', dataForm['sFiliale']);
        data.set('user', auth.getUserID());
        data.set('ville', dataForm['ville']);
        data.set('type', dataForm['type']);
        data.set('ressource', dataForm['ressource']);

        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Content-Type': 'application/merge-patch+json'
            }
        };
        const res = await axios.post(`${ process.env.NEXT_PUBLIC_API_URL }/societe`, data, config).then((response) => {
            return response;
        }).catch((error) => {
            return null;
        });

        if(res === null){
            this.setState({ 'error' : "Une erreur est survenue lors de l'envoi au serveur." });
        }else{
            this.setState({ 'success' : 'La société a bien été crée.' });
            auth.setCookie('societe',res.id);
            Router.pushRoute('compte');
        }
    }

    render(){
        const { alert } = this.state;

        return (
            <React.Fragment>
                <Head>
                    <title>Compléter mon Compte - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Compléter mon Compte</h1>

                    <AddFilialeForm alert={ alert } handleSubmit={ this._handleSubmit } />
                </Layout>
            </React.Fragment>
        )
    }
}
export default Compte;