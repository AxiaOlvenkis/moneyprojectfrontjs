import Layout from '../../components/Layout';
import {withRouter} from "next/router";
import axios from 'axios';
import React from "react";
import Head from "next/head";
import "./single.scss";

class Article extends React.Component {

	static getInitialProps = async ({query}) => {
		const res = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/noauth/articles/${query.id}`);
		const data = await res.data;

		return { details: data };
	};
  	render () {
		const { titre, contenu, createdAt } = this.props.details;
		const date = new Date(createdAt);

		return (
			<React.Fragment>
				<Head>
					<title>{ titre } - Money Project</title>
				</Head>
				<Layout>
					<h1>{ titre }</h1>
					<div className="singleActu">
						<h4 className="text-center">publié le { new Intl.DateTimeFormat("fr-FR", {
							year: "numeric",
							month: "long",
							day: "2-digit"
						}).format(date) } </h4>
						<div className="container" dangerouslySetInnerHTML={{ __html: contenu }} />
					</div>
				</Layout>
			</React.Fragment>
		)
	}
}
export default withRouter(Article)