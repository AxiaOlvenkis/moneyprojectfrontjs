import Layout from '../../components/Layout';
import React from "react";
import {withRouter} from "next/router";
import Head from "next/head";
import {Link} from "../../assets/helpers/routes";
import axios from 'axios';
import "./liste.scss";


class Actualites extends React.Component {

    static getInitialProps = async () => {
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };

        const res = await axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/noauth/articles`, headers);
        const data = await res.data;

        return { liste: data };
    };

    render = () => {
        const { liste } = this.props;

        return (
            <React.Fragment>
                <Head>
                    <title>Actualités - Money Project</title>
                </Head>
                <Layout>
                    <h1>Actualités</h1>
                    <ul className="listActu">
                        {liste.map(actu => {
                            const date = new Date(actu.createdAt);
                            return (
                                <li key={actu.id} className="single">
                                    <Link route='singleactualites' params={{id: actu.id}}>
                                        <a>
                                            <span className="title">{ actu.titre }</span>
                                            <span className="date">publié le
                                            {new Intl.DateTimeFormat("fr-FR", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(date)}</span>
                                        </a>
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
                </Layout>
            </React.Fragment>
        )
    }
}
export default withRouter(Actualites)