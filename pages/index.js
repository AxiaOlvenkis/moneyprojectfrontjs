import React from 'react';
import Layout from '../components/Layout';
import Head from "next/head";

export default class Accueil extends React.Component {
    render(){
        return (
            <React.Fragment>
                <Head>
                    <title>Accueil - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Money Project</h1>
                </Layout>
            </React.Fragment>
        )
    }
}