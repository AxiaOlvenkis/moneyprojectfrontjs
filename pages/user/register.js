import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "./forms.scss";
import axios from 'axios';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            username: "",
            password: "",
            rpassword: "",
            error: "",
            success: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    _handleSubmit = async event => {
        event.preventDefault();
        this.setState({
            'error' : '',
            'success' : ''
        });

        if(this.state.password !== this.state.rpassword){
            this.setState({ 'error' : "les mots de passes ne correspondent pas." });
        }else{
            // on envoie
            const data = {
                username: this.state.username,
                password: this.state.password,
                email: this.state.email
            };

            const res = await axios.post(`${ process.env.NEXT_PUBLIC_API_URL }/noauth/user/register`, data).then((response) => {
                return response;
            }).catch((error) => {
                return null;
            });

            if(res === null){
                this.setState({ 'error' : "Une erreur est survenue lors de l'envoi au serveur." });
            }else{
                const { data } = await res.data;
                this.setState({ 'success' : data });
            }
        }
    };

    render(){
        return (
            <React.Fragment>
                <Head>
                    <title>Inscription - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Inscription</h1>

                    <form className="container" onSubmit={this._handleSubmit}>
                        {
                            this.state.error !== "" ?
                                <div className="alert alert-danger" role="alert">
                                    { this.state.error }
                                </div>
                                :
                                ""
                        }

                        {
                            this.state.success !== "" ?
                                <div className="alert alert-success" role="alert">
                                    { this.state.success }
                                </div>
                                :
                                ""
                        }

                        <div className="row">
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="email">Email</label>
                                <input required type="email" className="form-control" id="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
                            </div>
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="pseudo">Pseudo</label>
                                <input required type="text" className="form-control" id="pseudo" name="username" value={this.state.username} onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="password">Mot de passe</label>
                                <input required type="password" className="form-control" id="password" name="password" value={this.state.password} onChange={this.handleInputChange} />
                            </div>
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="r_password">Repeter le mot de passe</label>
                                <input required type="password" className="form-control" id="r_password" name="rpassword" value={this.state.rpassword} onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary">S'enregistrer</button>
                    </form>
                </Layout>
            </React.Fragment>
        )
    }
}
export default Register;