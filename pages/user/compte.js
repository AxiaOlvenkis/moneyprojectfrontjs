import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "./forms.scss";
import AuthToken from "../../assets/helpers/auth_token";
import axios from 'axios';
import {Router} from "../../assets/helpers/routes";

class Compte extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            username: "",
            password: "",
            rpassword: "",
            error: "",
            success: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount(){
        const auth = new AuthToken();
        console.log(auth.isComplete());
        if(!auth.isAuth()){
            Router.pushRoute('login')
        }else if(auth.isComplete() === false){
            Router.pushRoute('complete_account');
        }

        // on set les infos
        const id = auth.getTokenID();
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
            }
        };
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/users/${id}`, config).then((response) => {
            const retour = response.data;

            this.setState({
                username: retour.username,
                email: retour.email
            })
        }).catch((error) => {
            this.setState({ 'error' : "Une erreur est survenue lors de l'authentification." });
            return null;
        });
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    _handleSubmit = async event => {
        event.preventDefault();
        this.setState({
            'error' : '',
            'success' : ''
        });

        if(this.state.password !== "" && this.state.password !== this.state.rpassword){
            this.setState({ 'error' : "les mots de passes ne correspondent pas." });
        }else{
            // on envoie
            const auth = new AuthToken();

            const data = {};
            /*if(this.state.username !== ""){
                data.username = this.state.username;
            }*/
            if(this.state.password !== ""){
                data.password = this.state.password;
            }
            if(this.state.email !== ""){
                data.email = this.state.email;
            }

            const id = auth.getUserID();
            const config = {
                headers: {
                    'Authorization': auth.authorizationString(),
                    'Content-Type': 'application/merge-patch+json'
                }
            };

            const res = await axios.patch(`${ process.env.NEXT_PUBLIC_API_URL }/users/${id}`, data, config).then((response) => {
                return response;
            }).catch((error) => {
                console.log(error);
                return null;
            });

            if(res === null){
                this.setState({ 'error' : "Une erreur est survenue lors de l'envoi au serveur." });
            }else{
                this.setState({ 'success' : 'Modifications bien enregistré' });
            }
        }
    };

    render(){
        return (
            <React.Fragment>
                <Head>
                    <title>Mon Compte - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Mon Compte</h1>

                    <form className="container" onSubmit={this._handleSubmit}>
                        {
                            this.state.error !== "" ?
                                <div className="alert alert-danger" role="alert">
                                    { this.state.error }
                                </div>
                                :
                                ""
                        }

                        {
                            this.state.success !== "" ?
                                <div className="alert alert-success" role="alert">
                                    { this.state.success }
                                </div>
                                :
                                ""
                        }

                        <div className="row">
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="email">Email</label>
                                <input type="email" className="form-control" id="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
                            </div>
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="pseudo">Pseudo</label>
                                <input readOnly type="text" className="form-control" id="pseudo" name="username" value={this.state.username} onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="password">Mot de passe</label>
                                <input type="password" className="form-control" id="password" name="password" value={this.state.password} onChange={this.handleInputChange} />
                            </div>
                            <div className="form-group col-12 col-lg-6">
                                <label htmlFor="r_password">Repeter le mot de passe</label>
                                <input type="password" className="form-control" id="r_password" name="rpassword" value={this.state.rpassword} onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary">S'enregistrer</button>
                    </form>
                </Layout>
            </React.Fragment>
        )
    }
}
export default Compte;