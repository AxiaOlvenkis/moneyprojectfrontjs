import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import axios from "axios";
import Cookie from "js-cookie";
import AuthToken from "../../assets/helpers/auth_token";
import {Router} from '../../assets/helpers/routes';
import "./forms.scss";

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            error: "",
            success: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount(){
        const auth = new AuthToken();
        console.log('login');
        console.log(auth.isAuth());
        if(auth.isAuth()){
            Router.pushRoute('compte');
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    _handleSubmit = async event => {
        event.preventDefault();
        this.setState({
            'error' : '',
            'success' : ''
        });

        // on envoie
        const data = new FormData();
        data.set('_username', this.state.username);
        data.set('_password', this.state.password);

        const res = await axios.post(`${ process.env.NEXT_PUBLIC_API_URL }/login_check`, data).then((response) => {
            return response;
        }).catch((error) => {
            return null;
        });

        if(res === null){
            this.setState({ 'error' : "Une erreur est survenue lors de l'envoi au serveur." });
        }else{
            const { token } = await res.data;
            Cookie.set('userToken', token);
            // on set les infos
            const auth = new AuthToken();
            const id = auth.getTokenID();
            const config = {
                headers: {
                    'Authorization': auth.authorizationString(),
                }
            };
            await axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/users/${id}`, config).then((response) => {
                const retour = response.data;
                this.setState({ 'success' : "connexion ok" });
                Cookie.set('id', retour.id);
                Cookie.set('userName', retour.username);

                if(retour.societes !== undefined)
                    Cookie.set('societe', retour.societes.id);

                Router.pushRoute('compte');
            }).catch((error) => {
                this.setState({ 'error' : "Une erreur est survenue lors de l'authentification." });
                Cookie.remove('userToken');
                return null;
            });
        }
    };

    render(){
        return (
            <React.Fragment>
                <Head>
                    <title>Se connecter - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Se connecter</h1>

                    <form className="container" onSubmit={this._handleSubmit}>
                        {
                            this.state.error !== "" ?
                                <div className="alert alert-danger" role="alert">
                                    { this.state.error }
                                </div>
                                :
                                ""
                        }

                        {
                            this.state.success !== "" ?
                                <div className="alert alert-success" role="alert">
                                    { this.state.success }
                                </div>
                                :
                                ""
                        }

                        <div className="row">
                            <div className="form-group col-12">
                                <label htmlFor="pseudo">Pseudo</label>
                                <input required type="text" className="form-control" id="pseudo" name="username" value={this.state.username} onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-12">
                                <label htmlFor="password">Mot de passe</label>
                                <input required type="password" className="form-control" id="password" name="password" value={this.state.password} onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary">Se Connecter</button>
                    </form>
                </Layout>
            </React.Fragment>
        )
    }
}
export default Login;