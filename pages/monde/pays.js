import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "../user/forms.scss";
import AuthToken from "../../assets/helpers/auth_token";
import axios from 'axios';
import {Router} from "../../assets/helpers/routes";
import SinglePays from '../../components/Pays/pays';

class Pays extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pays: null,
            single: null
        };
    }

    componentDidMount(){
        const auth = new AuthToken();
        if(!auth.isAuth()){
            Router.pushRoute('login')
        }else if(!auth.isComplete()){
            Router.pushRoute('complete_account');
        }

        // on recupere la societe
        // on set les infos
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/pays`, config).then((response) => {
            this.setState({
                pays: response.data
            });
        }).catch((error) => {
            return null;
        });
    }

    lineClick = (event, pays) => {
        const target = event.target.parentNode;

        if(target.className === "active"){
            target.className = "";
            this.setState({single: null});
        }else{
            target.className = "active";
            this.setState({single: pays});
        }
    }

    render(){
        const { pays, single } = this.state;

        return (
            <React.Fragment>
                <Head>
                    <title>Les Pays - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Les Pays</h1>

                    <div className="row">
                        <div className="col-12 col-lg-4">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!pays
                                    ? ""
                                    : pays.map((p, i) => {
                                        return (
                                            <tr key={ i } onClick={(event) => this.lineClick(event, p)}>
                                                <td>{ p.nom }</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>

                        <div className="col-12 col-lg-8">
                            {!single
                                ? ""
                                : <SinglePays pays={single} />
                            }
                        </div>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}
export default Pays;