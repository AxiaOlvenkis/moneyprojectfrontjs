import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "../user/forms.scss";
import AuthToken from "../../assets/helpers/auth_token";
import axios from 'axios';
import {Router} from "../../assets/helpers/routes";
import LineFiliale from "../../components/Market/LineFiliale";
import LineRessource from "../../components/Market/LineRessource";
import LineEmplacement from "../../components/Market/LineEmplacement";

class Filiales extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filiales: null,
            uSociety: null,
            error: "",
            success: ""
        };
    }

    componentDidMount(){
        const auth = new AuthToken();
        if(!auth.isAuth()){
            Router.pushRoute('login')
        }else if(!auth.isComplete()){
            Router.pushRoute('complete_account');
        }

        // on recupere les emplacements de ressources villes à vendre
        // on set les infos
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/market_filiales`, config).then((response) => {
            this.setState({
                'filiales': response.data
            });
        }).catch((error) => {
            return null;
        });

        const id = auth.getSociety();
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/societes/${id}`, config).then((response) => {
            this.setState({
                'uSociety': response.data
            });
        }).catch((error) => {
            return null;
        });
    }

    render(){
        const { filiales, uSociety } = this.state;

        return (
            <React.Fragment>
                <Head>
                    <title>Place de Marché - Filiale - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Place de Marché - Filiale</h1>

                    <div className="container-fluid">
                        <table className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Ville</th>
                                    <th>Nom</th>
                                    <th>Ressources</th>
                                    <th>Propriétaire</th>
                                    <th>Niveau</th>
                                    <th>Prix</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            {!filiales || !filiales.length
                                ? ""
                                : filiales.map((fil, index) => (
                                    <LineFiliale key={index} market={fil} uSociety={uSociety} />
                                ))}
                            </tbody>
                        </table>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}
export default Filiales;