import React from 'react';
import Layout from '../../components/Layout';
import Head from "next/head";
import "../user/forms.scss";
import AuthToken from "../../assets/helpers/auth_token";
import axios from 'axios';
import {Router} from "../../assets/helpers/routes";
import LineRessource from "../../components/Market/LineRessource";
import LineEmplacement from "../../components/Market/LineEmplacement";

class Emplacement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ressources: null,
            uSociety: null,
            error: "",
            success: ""
        };
    }

    componentDidMount(){
        const auth = new AuthToken();
        if(!auth.isAuth()){
            Router.pushRoute('login')
        }else if(!auth.isComplete()){
            Router.pushRoute('complete_account');
        }

        // on recupere les emplacements de ressources villes à vendre
        // on set les infos
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/ressources/market`, config).then((response) => {
            this.setState({
                'ressources': response.data
            });
        }).catch((error) => {
            return null;
        });

        const id = auth.getSociety();
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/societes/${id}`, config).then((response) => {
            this.setState({
                'uSociety': response.data
            });
        }).catch((error) => {
            return null;
        });
    }

    render(){
        const { ressources, uSociety } = this.state;

        return (
            <React.Fragment>
                <Head>
                    <title>Place de Marché - Ressources - Money Project</title>
                </Head>
                <Layout>
                    <h1 className="text-center">Place de Marché - Ressources</h1>

                    <div className="container-fluid">
                        <table className="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Ressource</th>
                                <th>Ville</th>
                                <th>Quantité</th>
                                <th>Prix Unitaire</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            {!ressources || !ressources.length
                                ? ""
                                : ressources.map((ress, index) => (
                                    <LineRessource key={index} market={ress} uSociety={uSociety} />
                                ))}
                            </tbody>
                        </table>
                    </div>
                </Layout>
            </React.Fragment>
        )
    }
}
export default Emplacement;