import React from "react";
import NumberFormat from 'react-number-format';
import { FaCommentDollar } from "react-icons/fa";

class LineFiliale extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            market: this.props.market,
            uSociety: this.props.uSociety
        }
    }

    render = () => {
        const { market, uSociety } = this.state;
        const filiale = market.filiale;
        const societe = filiale.societe;
        console.log(uSociety);

        return(
            <React.Fragment>
                <tr>
                    <td>{ filiale.type.nom }</td>
                    <td>{ filiale.ville.nom }</td>
                    <td>{ filiale.nom }</td>
                    <td>
                        {!filiale.ressourcesFiliales.length
                            ? ""
                            : filiale.ressourcesFiliales.map((r, index) => {
                                if(r.ressource){}else{
                                    return (
                                        <span key={index}>{ r.villeRessource.ressource.nom }</span>
                                    )
                                }
                            })}
                    </td>
                    <td>{ societe.user == null ? '/':societe.user.username }</td>
                    <td>{ filiale.niveau }</td>
                    <td><NumberFormat value={ market.prix } displayType={'text'} thousandSeparator={' '} suffix={' €'} decimalSeparator={','}/></td>
                    <td>
                        { uSociety.argent > market.prix ? <FaCommentDollar />:'' }
                    </td>
                </tr>
            </React.Fragment>
        );
    }
}

export default LineFiliale;