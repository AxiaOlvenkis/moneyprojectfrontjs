import React from "react";
import NumberFormat from 'react-number-format';
import {FaCommentDollar} from "react-icons/fa";

class LineEmplacement extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            market: this.props.market,
            uSociety: this.props.uSociety,
            price: 0
        }
    }

    componentDidMount() {
        const line = this.state.market;
        const qte = line.quantite !== 0 ? line.quantite:10000000;
        const price = qte * ( line.ressource.ratio / 100 ) * ( line.fertilite / 100);
        this.setState({ price: price })
    }

    render = () => {
        const { market, price, uSociety } = this.state;

        return(
            <React.Fragment>
                <tr>
                    <td>{ market.ville.nom }</td>
                    <td>{ market.ressource.nom }</td>
                    <td>
                        {
                            market.quantite === 0
                            ? '/'
                            : <NumberFormat value={ market.quantite } displayType={'text'} thousandSeparator={' '} suffix={' unité'} decimalSeparator={','}/>
                        }
                    </td>
                    <td><NumberFormat value={ market.ressource.ratio } displayType={'text'} thousandSeparator={' '} suffix={'%'} decimalSeparator={','}/></td>
                    <td><NumberFormat value={ market.fertilite } displayType={'text'} thousandSeparator={' '} suffix={'%'} decimalSeparator={','}/></td>
                    <td><NumberFormat value={ price } displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/></td>
                    <td>
                        { uSociety.argent > price ? <FaCommentDollar />:'' }
                    </td>
                </tr>
            </React.Fragment>
        );
    }
}

export default LineEmplacement;