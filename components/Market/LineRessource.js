import React from "react";
import NumberFormat from 'react-number-format';
import { FaCommentDollar } from "react-icons/fa";

class LineRessource extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            market: this.props.market,
            uSociety: this.props.uSociety
        }
    }

    render = () => {
        const { market } = this.state;

        return(
            <React.Fragment>
                <tr>
                    <td>{ market.ressource.nom }</td>
                    <td>{ market.ville.nom }</td>
                    <td><NumberFormat value={ market.quantite } displayType={'text'} thousandSeparator={' '} suffix={' unité'} decimalSeparator={','}/></td>
                    <td><NumberFormat value={ market.prixUnitaire } displayType={'text'} thousandSeparator={' '} suffix={' €'} decimalSeparator={','}/></td>
                    <td><FaCommentDollar /></td>
                </tr>
            </React.Fragment>
        );
    }
}

export default LineRessource;