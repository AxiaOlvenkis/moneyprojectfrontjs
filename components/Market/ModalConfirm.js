import Modal from "react-bootstrap/Modal";
import React from "react";
import Button from "react-bootstrap/Button";
import {IoIosArrowDroprightCircle} from "react-icons/io";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import AuthToken from "../../assets/helpers/auth_token";
import axios from "axios";
import NumberFormat from "react-number-format";
import RessourcesHelper from "../../assets/helpers/ressources";

class SettingsModal extends React.Component{
    constructor(props) {
        super(props);

        const prod = RessourcesHelper.calculProduction(this.props.type, this.props.filiale, this.props.ressource);

        this.state = {
            sInitialBudget: this.props.societe.argent,
            fInitialBudget: this.props.filiale.argent,
            sBudget: this.props.societe.argent,
            fBudget: this.props.filiale.argent,
            investissement: this.props.filiale.investissement,
            type: this.props.type,
            production: Math.round(prod)
        }

        this._onChangeBudget = this._onChangeBudget.bind(this);
    }

    _recalculProduction = (inv) => {
        this.props.filiale.investissement = inv;
        const prod = RessourcesHelper.calculProduction(this.props.type, this.props.filiale, this.props.ressource);

        this.setState({production: Math.round(prod)});
    }

    _onChangeBudget = (event) => {
        const nBudgFiliale = event.target.value - this.state.fInitialBudget;
        const newBudget = this.state.sInitialBudget - nBudgFiliale;

        if(newBudget >= 0){
            this.setState({
                sBudget: newBudget,
                fBudget: event.target.value
            });
        }else{
            event.target.value = this.state.sInitialBudget + this.state.fInitialBudget
            this.setState({
                sBudget: 0,
                fBudget: event.target.value
            });
        }
    }

    _onChangeInvestissement = (event) => {
        this.setState({
            investissement: event.target.value
        });

        this._recalculProduction(event.target.value);
    }

    _saveChangement = async() => {
        const { sBudget, fBudget, investissement } = this.state;
        const sid = this.props.societe.id;
        const fid = this.props.filiale.id;

        // on set le header
        const auth = new AuthToken();
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Content-Type': 'application/merge-patch+json'
            }
        };

        const data = {};
        data.argent = Number(sBudget);

        // on met à jour la societe, et si c'est OK on met à jour la filiale.
        await axios.patch(`${ process.env.NEXT_PUBLIC_API_URL }/societes/${sid}`, data, config).then((response) => {
            // on met à jour la filiale
            const dataS = {};
            dataS.argent = Number(fBudget);
            dataS.investissement = Number(investissement);

            axios.patch(`${ process.env.NEXT_PUBLIC_API_URL }/societe_filiales/${fid}`, dataS, config).then((response) => {
                window.location.reload(false);
            }).catch((error) => {
                console.log(error);
                return null;
            });
        }).catch((error) => {
            console.log(error);
            return null;
        });

    }

    render = () => {
        const {sBudget, fBudget, investissement, production } = this.state;

        return (
            <Modal  {...this.props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Paramétres de la Filiale
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Modifier le Budget</h4>
                    <div className="row">
                        <div className="form-group col-5">
                            <InputGroup className="mb-3">
                                <FormControl readOnly className="form-control" type="number" value={ sBudget }/>
                                <InputGroup.Append>
                                    <InputGroup.Text id="sBudget-group">€</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                        <div className="col-2 text-center">
                            <IoIosArrowDroprightCircle className="text-danger lead" />
                        </div>
                        <div className="form-group col-5">
                            <InputGroup className="mb-3">
                                <FormControl onChange={this._onChangeBudget} className="form-control" type="number" defaultValue={ fBudget }/>
                                <InputGroup.Append>
                                    <InputGroup.Text id="fBudget-group">€</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </div>
                    <h4>Modifier l'investissement (par jour)</h4>
                    <div className="form-group col-12">
                        <InputGroup className="mb-3">
                            <FormControl type="number" defaultValue={ investissement } onChange={this._onChangeInvestissement}/>
                            <InputGroup.Append>
                                <InputGroup.Text id="investissement-group">€</InputGroup.Text>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                    <h4>Simulation de la production maximale journaliere</h4>
                    <p className="h3 text-center"><NumberFormat value={ production } displayType={'text'} thousandSeparator={ ' ' } suffix={' unités'} decimalSeparator={','}/></p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={ this._saveChangement }>Sauvegarder</Button>
                    <Button onClick={this.props.onHide}>Fermer</Button>
                </Modal.Footer>
            </Modal>
        )
    };
}

export default SettingsModal;