import React from "react";

class Alert extends React.Component{
    constructor(props) {
        super(props);
    }

    render = () => {
        const { alert } = this.props;

        return(
            <React.Fragment>
                {
                    !alert
                    ? null
                    : <div className={ `alert alert-${ alert.classe }` } role="alert" dangerouslySetInnerHTML={{ __html: alert.message }} />
                }
            </React.Fragment>
        )
    }
}

export default Alert;