import React from "react";
import NumberFormat from 'react-number-format';

class Societe extends React.Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const { nom, argent } = this.props.societe;

        return (
            <React.Fragment>
                <div className="row text-center">
                    <div className="col-12 col-lg-6">
                        <h2>Société <b className="text-primary">{ nom }</b></h2>
                    </div>
                    <div className="col-12 col-lg-6">
                        <p className="h2">
                            <b>Budget</b> :
                            <span className="text-danger">
                                <NumberFormat value={argent} displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/>
                            </span>
                        </p>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Societe