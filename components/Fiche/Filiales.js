import React from "react";
import NumberFormat from 'react-number-format';
import './Fiche.scss';

class Filiales extends React.Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {argent, niveau, nom, ville, type, activeProduction } = this.props;
        const production = activeProduction.ressourcesSortantes

        return (
            <React.Fragment>
                <tr onClick={this.props.lineClick}>
                    <td className="align-middle">{ nom }</td>
                    <td className="align-middle text-bold text-danger"><NumberFormat value={argent} displayType={'text'} thousandSeparator={ ' ' } suffix={'€'} decimalSeparator={','}/></td>
                    <td>{ ville.nom }</td>
                    <td>{ type.nom }</td>
                    <td>{ niveau }</td>
                    <td>{ production.nom }</td>
                </tr>
            </React.Fragment>
        )
    }
}
export default Filiales