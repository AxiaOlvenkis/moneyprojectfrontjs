import React from "react";
import NumberFormat from 'react-number-format';
import SettingsModal from "./FilialeSettings";
import { IoIosArrowDroprightCircle, IoIosSettings, IoIosArrowRoundBack } from "react-icons/io";
import RessourcesHelper from "../../assets/helpers/ressources";

class Filiale extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modalShow: false,
            estimation: 0
        }
    }

    componentDidMount = () => {
        const { filiale } = this.props;
        const prod = RessourcesHelper.calculProduction(filiale.type, filiale, filiale.ressourcesFiliales[0]);
        this.setState({
            estimation: prod
        });
    }


    render = () => {
        const {argent, investissement, niveau, nom, ville, production, type, activeProduction, ressourcesFiliales } = this.props.filiale;
        const { estimation } = this.state;

        return (
            <React.Fragment>
                <div id="old">
                    <div className="row justify-content-center filiale-header">
                        <IoIosArrowRoundBack onClick={ this.props.backClick } className="back-btn" />
                        <h2 className="h1 text-center">{ nom }</h2>
                        <span className="badge badge-info align-self-baseline h5">{ niveau }</span>
                        <span className="align-self-baseline h5"><IoIosSettings onClick={() => this.setState({modalShow: true})} /></span>
                    </div>

                    <div className="row text-center">
                        <div className="card col-12 col-lg-6">
                            <div className="card-body">
                                <p><b>Pays</b> :{ ville.region.pays.nom }</p>
                                <p><b>Région</b> :{ ville.region.nom }</p>
                                <p><b>Ville</b> :{ ville.nom }</p>
                            </div>
                        </div>
                        <div className="card col-12 col-lg-6">
                            <div className="card-body">
                                <p>
                                    <b>Budget actuel</b> :
                                    <span className="text-bold text-danger">
                                    <NumberFormat value={argent} displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/>
                                </span>
                                </p>
                                <p>
                                    <b>Investissement</b> :
                                    <span className="text-bold text-danger">
                                <NumberFormat value={investissement} displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/>
                            </span>
                                </p>
                            </div>
                        </div>
                        <div className="card col-12 col-lg-6">
                            <div className="card-body">
                                <h5 className="text-info text-bold">{ type.nom }</h5>
                                <table className="table table-bordered">
                                    <tbody>
                                    {!ressourcesFiliales.length
                                        ? ""
                                        : ressourcesFiliales.map((r, index) => {
                                            if(r.ressource){}else{
                                                return (
                                                    <tr key={index}>
                                                        <td>{ r.villeRessource.ressource.nom }</td>
                                                        <td>Fertilité:{ r.villeRessource.fertilite }%</td>
                                                        <td>Ratio:{ r.villeRessource.ressource.ratio }%</td>
                                                        <td>Ratio { type.nom }: { type.ratioProduction }%</td>
                                                    </tr>
                                                )
                                            }
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="card col-12 col-lg-6">
                            <div className="card-body">
                                <h5>Production Actuelle</h5>
                                <div className="row text-center">
                                    <div className="col-2">-</div>
                                    <div className="col-4">
                                        {!activeProduction.ressourcesEntrantes.length
                                            ? ""
                                            : activeProduction.ressourcesEntrantes.map((r, index) => (
                                                <span key={index}>{ r.nom }</span>
                                            ))}
                                    </div>
                                    <div className="col-2">
                                        <IoIosArrowDroprightCircle className="text-danger lead" />
                                    </div>
                                    <div className="col-4">
                                        <span>{ activeProduction.ressourcesSortantes.nom }</span>
                                    </div>
                                </div>
                                <div className="row text-center">
                                    <div className="col-2"><b>Stock</b></div>
                                    <div className="col-4">
                                    <span className="text-bold text-danger">
                                        <NumberFormat value={ 0 } displayType={'text'} thousandSeparator={' '} suffix={' unités'} decimalSeparator={','}/>
                                    </span>
                                    </div>
                                    <div className="col-2">-</div>
                                    <div className="col-4">
                                    <span className="text-bold text-danger">
                                        <NumberFormat value={production} displayType={'text'} thousandSeparator={' '} suffix={' unités'} decimalSeparator={','}/>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row justify-content-center filiale-header">
                    <IoIosArrowRoundBack onClick={ this.props.backClick } className="back-btn" />
                    <h2 className="h1 text-center">{ nom }, <small>{ type.nom } de niveau { niveau }</small></h2>
                </div>

                <div className="row text-center">
                    <div className="card col-12 col-lg-6">
                        <div className="card-body">
                            <p><b>Pays</b> :{ ville.region.pays.nom }</p>
                            <p><b>Région</b> :{ ville.region.nom }</p>
                            <p><b>Ville</b> :{ ville.nom }</p>
                        </div>
                    </div>
                    <div className="card col-12 col-lg-6">
                        <div className="card-body">
                            <p onClick={() => this.setState({modalShow: true})}><IoIosSettings /> Modifier les paramétres de la filiale</p>
                            <p>
                                <b>Budget actuel</b> :
                                <span className="text-bold text-danger">
                                    <NumberFormat value={argent} displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/>
                                </span>
                            </p>
                            <p>
                                <b>Investissement</b> :
                                <span className="text-bold text-danger">
                                <NumberFormat value={investissement} displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/>
                            </span>
                            </p>
                        </div>
                    </div>
                    <div className="card col-12 col-lg-6">
                        <div className="card-body">
                            <h5 className="text-info text-bold">Emplacement de ressources appartenant à la filiale :</h5>
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th />
                                        <th>Fertilité</th>
                                        <th>Ratio de production de l'emplacement</th>
                                        <th>Ratio de production { type.nom }</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!ressourcesFiliales.length
                                    ? ""
                                    : ressourcesFiliales.map((r, index) => {
                                        if(r.ressource){}else{
                                            return (
                                                <tr key={index}>
                                                    <td>{ r.villeRessource.ressource.nom }</td>
                                                    <td>{ r.villeRessource.fertilite }%</td>
                                                    <td>{ r.villeRessource.ressource.ratio }%</td>
                                                    <td>{ type.ratioProduction }%</td>
                                                </tr>
                                            )
                                        }
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="card col-12 col-lg-6">
                        <div className="card-body">
                            <h5>Stock de ressources</h5>
                            <hr />
                            <h5>Production estimé (jour suivant)</h5>
                            <p className="text-center">
                                <span>{ activeProduction.ressourcesSortantes.nom } : </span>
                                <span className="text-bold text-danger">
                                    <NumberFormat value={ estimation } displayType={'text'} thousandSeparator={' '} suffix={' unités'} decimalSeparator={','}/>
                                </span>
                            </p>
                            <hr />
                            <h5>Production (jour actuel)</h5>
                            <table className="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td width="40%">
                                            {!activeProduction.ressourcesEntrantes.length
                                                ? ""
                                                : activeProduction.ressourcesEntrantes.map((r, index) => (
                                                    <span key={index}>{ r.nom }</span>
                                                ))}
                                        </td>
                                        <td rowSpan="2" width="20%" className="align-middle">
                                            <IoIosArrowDroprightCircle className="text-danger lead" />
                                        </td>
                                        <td width="40%">
                                            <span>{ activeProduction.ressourcesSortantes.nom }</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <SettingsModal
                    show={this.state.modalShow}
                    onHide={() => this.setState({modalShow: false})}
                    societe={ this.props.societe }
                    filiale={ this.props.filiale }
                    type={ type }
                    ressource={ ressourcesFiliales[0] }
                    onUpdate={ this.props.onUpdate }
                />
            </React.Fragment>
        )
    }
}
export default Filiale