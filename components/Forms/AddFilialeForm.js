import React from 'react';
import Alert from '../Tools/Alert';
import RessourcesHelper from "../../assets/helpers/ressources";
import AuthToken from "../../assets/helpers/auth_token";
import {Router} from "../../assets/helpers/routes";
import axios from "axios";

class AddFilialeForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            types: [],
            villes: [],
            ressources: [],
            sSociete: "",
            sFiliale: "",
            sVille: "",
            sTypeObject: null,
            sType: "",
            sRessource: 0,
            ids: [],
            alert: null,
            dataForm: []
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.handleVilleChange = this.handleVilleChange.bind(this);
        this.handleRessourceChange = this.handleRessourceChange.bind(this);
    }

    componentDidMount(){
        const auth = new AuthToken();
        if(!auth.isAuth()){
            Router.pushRoute('login')
        }else{
            // on set les infos
            const config = {
                headers: {
                    'Authorization': auth.authorizationString(),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            };
            axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/types_entreprises`, config).then((response) => {
                this.setState({types: response.data});
            }).catch((error) => {
                return null;
            });
        }
    }

    isPrimaryRessources(id){
        // TODO : temporaires
        return id !== 3;
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let data = this.state.dataForm;
        data[name] = value;

        this.setState({
            [name]: value,
            dataForm: data
        });
    }

    handleTypeChange(event){
        const target = event.target;
        const value = target.value;
        const type = this.state.types[value];
        let data = this.state.dataForm;
        data['type'] = type.id;

        this.setState({
            sTypeObject: type,
            sType: type.id,
            villes: [],
            ressources: [],
            dataForm: data
        });

        if(RessourcesHelper.isPrimaryRessources(type.id)){
            /** on effectue la recherche des emplacements disponibles */
            this.loadFreeCities(type);
        }else{
            /** on affiche les villes */
            this.loadAllCities();
        }
    }

    loadAllCities(){
        const auth = new AuthToken();
        // on set les infos
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };
        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/villes`, config).then((response) => {
            this.setState({villes: response.data});
        }).catch((error) => {
            return null;
        });
    }

    loadFreeCities(type){
        const auth = new AuthToken();
        // on set les infos
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };

        const ids = type.productions.map(prod => {
            return prod.ressourcesEntrantes.map(r => {
                return r.id;
            })[0]
        });

        const strids = ids.join(',');

        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/villes/free?ids=${strids}`, config).then((response) => {
            this.setState({villes: response.data, ids: ids});
        }).catch((error) => {
            return null;
        });
    }

    handleVilleChange(event){
        const target = event.target;
        const value = target.value;
        const ville = this.state.villes[value];
        const type = this.state.sTypeObject;
        let data = this.state.dataForm;
        data['ville'] = ville.id;

        const auth = new AuthToken();
        // on set les infos
        const config = {
            headers: {
                'Authorization': auth.authorizationString(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };

        const ids = type.productions.map(prod => {
            return prod.ressourcesEntrantes.map(r => {
                return r.id;
            })[0]
        });

        const strids = ids.join(',');

        axios.get(`${ process.env.NEXT_PUBLIC_API_URL }/villes/ressources/free?ids=${strids}&ville=${ ville.id }`, config).then((response) => {
            this.setState({
                ressources: response.data,
                dataForm: data
            });
        }).catch((error) => {
            return null;
        });
    }

    handleRessourceChange(event){
        const target = event.target;
        const value = target.value;
        const ress = this.state.ressources[value];
        let data = this.state.dataForm;
        data['ressource'] = ress.id;

        this.setState({
            sRessource: ress.id,
            dataForm: data
        });
    }

    render(){
        const { alert, societe } = this.props;
        const { types, villes, ressources, ids, dataForm } = this.state;

        return (
            <React.Fragment>
                <form className="container" onSubmit={ (event) => this.props.handleSubmit(event, dataForm)}>
                    <Alert alert={ alert } />

                    <div className="row">
                        {
                            !societe
                            ?
                                <div className="form-group col-12 col-lg-6">
                                    <label htmlFor="societe">Nom de votre Société</label>
                                    <input type="text" className="form-control" id="societe" name="sSociete" value={this.state.sSociete || ''} onChange={this.handleInputChange} />
                                </div>
                            : null
                        }
                        <div className="form-group col-12 col-lg-6">
                            <label htmlFor="filiale">Nom de votre nouvelle filiale</label>
                            <input type="text" className="form-control" id="filiale" name="sFiliale" value={this.state.sFiliale || ''} onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group col-12 col-lg-6">
                            <label htmlFor="type">Type d'entreprise</label>
                            <select defaultValue="-1" className="form-control" name="sType" onChange={this.handleTypeChange}>
                                <option value="-1">Faites votre choix</option>
                                {types.map((type, index) => {
                                    return (
                                        <option key={type.id} value={ index }>{ type.nom }</option>
                                    )
                                })}
                            </select>
                        </div>
                    </div>

                    <div className="table-responsive">
                        <h3>Dans quelle ville voulez-vous l'installer ?</h3>
                        <table className="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Région</th>
                                <th>Pays</th>
                                <th>Population</th>
                                <th>Budget</th>
                            </tr>
                            </thead>

                            <tbody>
                            {villes.map((ville, index) => {
                                return (
                                    <tr key={ville.id}>
                                        <td>{ ville.nom }</td>
                                        <td>{ ville.region.nom }</td>
                                        <td>{ ville.region.pays.nom }</td>
                                        <td>{ ville.population }</td>
                                        <td>{ ville.argent }</td>
                                        <td><input name="sVille" type="radio" value={index} className="form-control" onChange={this.handleVilleChange} /></td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>

                    <div className="table-responsive">
                        <h3>Choisissez la ressource à exploiter</h3>
                        <table className="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Ratio de Production</th>
                                <th>Fertilité Emplacement</th>
                                <th>Quantité exploitable</th>
                                <th>Epuisable ?</th>
                            </tr>
                            </thead>

                            <tbody>
                            {ressources.map((r, index) => {
                                return (
                                    <tr key={r.id}>
                                        <td>{ r.ressource.nom }</td>
                                        <td>{ r.ressource.ratio }</td>
                                        <td>{ r.fertilite }</td>
                                        <td>{ r.quantite !== 0 ? r.quantite:"/" }</td>
                                        <td>{ r.ressource.epuisable ? 'Oui':'Non' }</td>
                                        <td><input name="sRessource" type="radio" value={index} className="form-control" onChange={this.handleRessourceChange} /></td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>

                    <button type="submit" className="btn btn-primary">{ societe ? "Créer la filiale":"S'enregistrer" }</button>
                </form>
            </React.Fragment>
        )
    }
}

export default AddFilialeForm;