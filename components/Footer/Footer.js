import {Link} from '../../assets/helpers/routes';
import './Footer.scss';

const Footer = () => (
    <footer className="row no-gutters">
        <div className="col-12 col-lg-6 copyright">
            <span>Copyright © Lart'Moukis - { new Date().getFullYear() }</span>
        </div>
        <div className="footerMenu">
            <ul>
                <li>
                    <Link route='pagecms' params={{slug: 'mentions-legales'}}>
                        <a>Mentions Légales</a>
                    </Link>
                </li>
            </ul>
        </div>
    </footer>
);

export default Footer;