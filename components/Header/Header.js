import {Link} from '../../assets/helpers/routes';
import './Header.scss';
import CustomMenu from "./CustomMenu";
import UserMenu from "./UserMenu";
import React from "react";
import { IoIosHome } from "react-icons/io";
import AuthToken from "../../assets/helpers/auth_token";

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render = () => {
        const auth = new AuthToken();

        return(
            <header className="">
                <ul className="classicMenu">
                    <li><a href="/"><IoIosHome /></a></li>
                    <li className="hasSubMenu">
                        <span>Le Jeu</span>
                        <ul className="sub-menu">
                            <CustomMenu route='lstactualites' title='Les Actualités' />
                            <CustomMenu route='pagecms' params={{slug: 'qui-sommes-nous'}} title='Informations' />
                            <CustomMenu route='changelog' title='Changelog' />
                            <CustomMenu route='todo' title='Todo Liste' />
                        </ul>
                    </li>
                    {
                        !auth.isAuth()
                        ? null
                        :
                            <React.Fragment>
                                <li className="hasSubMenu">
                                    <span>Monde</span>
                                    <ul className="sub-menu">
                                        <CustomMenu route='pays' private="true" title='Les Pays' />
                                    </ul>
                                </li>
                                <li className="hasSubMenu">
                                    <span>Ma Société</span>
                                    <ul className="sub-menu">
                                        <CustomMenu route='dashboard' private="true" title='Tableau de Bord' />
                                    </ul>
                                </li>
                                <li className="hasSubMenu">
                                    <span>Place de Marché</span>
                                    <ul className="sub-menu">
                                        <CustomMenu route='marketemplacement' private="true" title='Emplacements' />
                                        <CustomMenu route='marketressources' private="true" title='Ressources' />
                                        <CustomMenu route='marketfiliales' private="true" title='Filiales' />
                                    </ul>
                                </li>
                            </React.Fragment>
                    }
                </ul>
                <UserMenu />
            </header>
        )
    }
}

export default Header;