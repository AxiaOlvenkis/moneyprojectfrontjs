import React from "react";
import AuthToken from "../../assets/helpers/auth_token";
import {Link} from "../../assets/helpers/routes";

class UserMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render = () => {
        const auth = new AuthToken();
        if(auth.isAuth()){
            return (
                <div className="userMenu">
                    <Link route='compte'>
                        <a>Welcome { auth.getFullName() }</a>
                    </Link>
                </div>
            )
        }else{
            return (
                <ul className="userMenu">
                    <li>
                        <Link route='register'>
                            <a>S'inscrire</a>
                        </Link>
                    </li>
                    <li>
                        <Link route='login'>
                            <a>Connexion</a>
                        </Link>
                    </li>
                </ul>
            )
        }
    }
}
export default UserMenu