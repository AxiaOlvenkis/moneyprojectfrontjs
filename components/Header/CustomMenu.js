import React from "react";
import AuthToken from "../../assets/helpers/auth_token";
import {Link} from "../../assets/helpers/routes";

class CustomMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        if(this.props.params === undefined){
            this.state.params = {};
        }else{
            this.state.params = this.props.params;
        }

        if(this.props.private === undefined){
            this.state.private = false;
        }else{
            this.state.private = this.props.private;
        }
    }

    render = () => {
        const auth = new AuthToken();

        if((this.state.private !== false && auth.isAuth() !== false) || this.state.private === false){
            return (
                <li>
                    <Link route={ this.props.route } params={ this.state.params }>{ this.props.title }</Link>
                </li>
            )
        }else{
            return null;
        }
    }
}
export default CustomMenu