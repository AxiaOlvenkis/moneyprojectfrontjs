import React from "react";
import AuthToken from "../../assets/helpers/auth_token";
import {Router} from "../../assets/helpers/routes";
import axios from "axios";
import NumberFormat from "react-number-format";

class SingleVille extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ressources: null,
            ville: null
        }
    }

    componentDidMount(){
        const { ville } = this.props;
        const array = [];

        ville.villeRessources.forEach((item) => {
            console.log(item);
            const ress = item.ressource;
            if(array[ress.id]){
                array[ress.id]['nombre'] += 1;
                array[ress.id]['dispo'] = 0;
            }else{
                array[ress.id] = [];
                array[ress.id]['nom'] = ress.nom;
                array[ress.id]['nombre'] = 1;
                array[ress.id]['dispo'] = 0;
            }

            if(!item.prodFiliales){
                array[ress.id]['dispo'] += 1;
            }
        });

        this.setState({
            ressources: array,
            ville: ville
        });
    }

    render = () => {
        const { ville, ressources } = this.state;

        if(!ville){
            return null;
        }else{
            return (
                <React.Fragment>
                    <div className="row justify-content-center">
                        <div className="col-12">
                            <h2 className="h1 text-center">Ville de { ville.nom }</h2>

                            <p className="text-center"><NumberFormat value={ ville.population } displayType={'text'} thousandSeparator={' '} decimalSeparator={','}/> Habitants</p>
                            <p className="text-center"><NumberFormat value={ ville.argent } displayType={'text'} thousandSeparator={' '} suffix={'€'} decimalSeparator={','}/> de Budget</p>

                            <h4 className="text-center">Ressources :</h4>
                            {!ressources
                                ? ""
                                : ressources.map((r, i) => {
                                    return (
                                        <p key={ i } className="text-center">
                                            { r.nombre } parcelles de { r.nom }, dont { r.dispo } emplacement libre
                                        </p>
                                    )
                                })}
                        </div>
                    </div>
                </React.Fragment>
            )
        }
    }
}
export default SingleVille