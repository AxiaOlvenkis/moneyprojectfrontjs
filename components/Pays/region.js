import React from "react";
import SingleVille from "./ville";

class SingleRegion extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ville: null,
            single: null
        };
    }

    lineClick = (event, ville) => {
        const target = event.target.parentNode;

        if(target.className === "active"){
            target.className = "";
            this.setState({ville: null});
        }else{
            target.className = "active";
            this.setState({ville: ville});
        }
    }

    render = () => {
        const { nom, villes } = this.props.region;
        const { ville } = this.state;

        return (
            <React.Fragment>
                <div className="row justify-content-center">
                    <div className="col-12 col-lg-4">
                        <h2 className="h1 text-center">Région de { nom }</h2>

                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nom</th>
                            </tr>
                            </thead>
                            <tbody>
                            {!villes
                                ? ""
                                : villes.map((v, i) => {
                                    return (
                                        <tr key={ i } onClick={(event) => this.lineClick(event, v)}>
                                            <td>{ v.nom }</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                    <div className="col-12 col-lg-8">
                        {
                            ville ?
                                <SingleVille ville={ ville } />
                                :null
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default SingleRegion