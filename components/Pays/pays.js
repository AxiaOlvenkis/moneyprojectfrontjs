import React from "react";
import SingleRegion from "./region";

class SinglePays extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            regions: null,
            single: null
        };
    }

    lineClick = (event, region) => {
        const target = event.target.parentNode;

        if(target.className === "active"){
            target.className = "";
            this.setState({region: null});
        }else{
            target.className = "active";
            this.setState({region: region});
        }
    }

    render = () => {
        const { nom, regions } = this.props.pays;
        const { region } = this.state;

        return (
            <React.Fragment>
                <div className="row justify-content-center">
                    <div className="col-12 col-lg-4">
                        <h2 className="h1 text-center">{ nom }</h2>

                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nom</th>
                            </tr>
                            </thead>
                            <tbody>
                            {!regions
                                ? ""
                                : regions.map((r, i) => {
                                    return (
                                        <tr key={ i } onClick={(event) => this.lineClick(event, r)}>
                                            <td>{ r.nom }</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                    <div className="col-12 col-lg-8">
                        {
                            region ?
                                <SingleRegion region={ region } />
                                :null
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default SinglePays