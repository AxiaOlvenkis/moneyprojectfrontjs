// next.config.js
const withSass = require('@zeit/next-sass');
const withRoutes = require('next-routes')({
    Link: require('./assets/helpers/routes'),
    Router: require('./assets/helpers/routes')
});

module.exports = withSass(withRoutes);