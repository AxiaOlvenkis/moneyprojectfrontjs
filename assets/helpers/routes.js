const routes = require('next-routes');

// nom / pattern / page
module.exports = routes()
    .add('homepage', '/', 'index')
    .add('lstactualites', '/actualites', 'actualites/liste')
    .add('singleactualites', '/actualites/:id', 'actualites/single')
    .add('pagecms', '/cms/:slug', 'cms/single')
    .add('changelog', '/changelog', 'todo/changelog')
    .add('todo', '/todo', 'todo/todo')
    .add('register', '/inscription', 'user/register')
    .add('login', '/connexion', 'user/login')
    .add('logout', '/logout', 'user/logout')
    .add('compte', '/compte', 'user/compte')
    .add('complete_account', '/complete/account', 'generate/complete')
    .add('dashboard', '/dashboard', 'entreprises/dashboard')
    .add('create-filiale', '/filiale/create', 'generate/createFiliale')
    .add('marketemplacement', '/market/emplacement', 'market/emplacement')
    .add('marketressources', '/market/ressources', 'market/ressources')
    .add('marketfiliales', '/market/filiales', 'market/filiales')
    .add('pays', '/pays', 'monde/pays');