export default class RessourcesHelper {
    static defaultPrice = 2;
    static defaultBrutProduction = 10000;
    static defaultManufacturedProduction = 15000;
    static defaultInvBrut = 0.001;
    static defaultInvManufactured = 0.04;

    static isPrimaryRessources(id){
        // TODO : temporaires
        return id !== 3;
    }

    static calculProduction(type, filiale, ressource){
        if(RessourcesHelper.isPrimaryRessources(type.id)){
            return RessourcesHelper.calculBrutProduction(type, filiale, ressource.villeRessource);
        }else{
            return RessourcesHelper.calculManufaturedProduction();
        }
    }

    static calculBrutProduction(type, filiale, vRessource){
        const percentEntreprise = type.ratioProduction / 100;
        const investissement = filiale.investissement * RessourcesHelper.defaultInvBrut;
        const percentRessource = (investissement + vRessource.fertilite + vRessource.ressource.ratio) / 100;
        const fRatio = percentEntreprise * percentRessource;

        return RessourcesHelper.defaultBrutProduction * fRatio;
    }

    static calculManufaturedProduction(type, inv, ratio, niveau){
        // TODO : finaliser
        const percentEntreprise = type.ratio_production / 100;
        const investissement = inv * RessourcesHelper.defaultInvManufactured;
        const percentRessource = ratio / 100;
        const fRatio = percentEntreprise * percentRessource * investissement;
        const max = ( niveau * 10) * RessourcesHelper.defaultManufacturedProduction;

        return max * fRatio;
    }
}