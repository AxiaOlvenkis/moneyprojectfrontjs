import jwtDecode from "jwt-decode";
import Cookie from "js-cookie";

export default class AuthToken {
    decodedToken = { username: "", exp: 0 };

    constructor() {
        this.token = Cookie.get('userToken');

        // then try and decode the jwt using jwt-decode
        try {
            if (this.token) this.decodedToken = jwtDecode(this.token);
        } catch (e) {
        }
    }

    authorizationString() {
        return `Bearer ${this.token}`;
    }

    isAuth(){
        if(Cookie.get("userToken") === null || Cookie.get("userName") === null || !this.isValid()){
            return false;
        }else{
            return Cookie.get("userName");
        }
    }

    expiresAt(){
        return new Date(this.decodedToken.exp * 1000);
    }

    isExpired(){
        return new Date() > this.expiresAt();
    }

    isValid(){
        return !this.isExpired()
    }

    getUsername(){
        return this.decodedToken.username;
    }

    getTokenID(){
        return this.decodedToken.id;
    }

    getFullName(){
        return Cookie.get("userName");
    }

    getUserID(){
        return Cookie.get("id");
    }

    getSociety(){
        return Cookie.get('societe');
    }

    isComplete(){
        const soc = Cookie.get("societe");

        return !(soc === undefined || soc === null);
    }

    setCookie(key, value){
        Cookie.set(key, value);
    }
}